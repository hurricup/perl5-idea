Reparsing block
----------
Full reparse
----------
After typing
----------
if($a){
  for(my $i = 0; i < 10; }<caret> ){
    say 'hi'
  }
}

----------
Psi structure
----------
Perl5
  PsiPerlIfCompoundImpl(Perl5: IF_COMPOUND)
    PsiElement(Perl5: if)('if')
    PsiPerlConditionalBlockImpl(Perl5: CONDITIONAL_BLOCK)
      PsiPerlConditionExprImpl(Perl5: CONDITION_EXPR)
        PsiElement(Perl5: ()('(')
        PsiPerlScalarVariableImpl(Perl5: SCALAR_VARIABLE)
          PsiElement(Perl5: $$)('$')
          PerlVariableNameElementImpl(Perl5: SCALAR_NAME)('a')
        PsiElement(Perl5: ))(')')
      PsiPerlBlockImpl(Perl5: BLOCK)
        PsiElement(Perl5: {)('{')
        PsiWhiteSpace('\n  ')
        PsiPerlForCompoundImpl(Perl5: FOR_COMPOUND)
          PsiElement(Perl5: for)('for')
          PsiElement(Perl5: ()('(')
          PsiPerlForInitImpl(Perl5: FOR_INIT)
            PsiPerlAssignExprImpl(Perl5: ASSIGN_EXPR)
              PsiPerlVariableDeclarationLexicalImpl(Perl5: VARIABLE_DECLARATION_LEXICAL)
                PsiElement(Perl5: my)('my')
                PsiWhiteSpace(' ')
                PsiPerlVariableDeclarationElementImpl(VARIABLE_DECLARATION_ELEMENT)
                  PsiPerlScalarVariableImpl(Perl5: SCALAR_VARIABLE)
                    PsiElement(Perl5: $$)('$')
                    PerlVariableNameElementImpl(Perl5: SCALAR_NAME)('i')
              PsiWhiteSpace(' ')
              PsiElement(Perl5: =)('=')
              PsiWhiteSpace(' ')
              PsiPerlNumberConstantImpl(Perl5: NUMBER_CONSTANT)
                PsiElement(Perl5: NUMBER)('0')
          PsiElement(Perl5: ;)(';')
          PsiWhiteSpace(' ')
          PsiPerlForConditionImpl(Perl5: FOR_CONDITION)
            PsiPerlCompareExprImpl(Perl5: COMPARE_EXPR)
              PsiPerlSubCallImpl(SUB_CALL)
                PsiPerlMethodImpl(Perl5: METHOD)
                  PerlSubNameElementImpl(Perl5: subname)('i')
              PsiWhiteSpace(' ')
              PsiElement(Perl5: <)('<')
              PsiWhiteSpace(' ')
              PsiPerlNumberConstantImpl(Perl5: NUMBER_CONSTANT)
                PsiElement(Perl5: NUMBER)('10')
          PsiElement(Perl5: ;)(';')
          PsiErrorElement:<for mutator> or Perl5: ) expected, got '}'
            <empty list>
        PsiWhiteSpace(' ')
        PsiElement(Perl5: })('}')
  PsiWhiteSpace(' ')
  PsiErrorElement:')' unexpected
    PsiElement(Perl5: ))(')')
  PsiPerlBlockCompoundImpl(Perl5: BLOCK_COMPOUND)
    PsiPerlBlockImpl(Perl5: BLOCK)
      PsiElement(Perl5: {)('{')
      PsiWhiteSpace('\n    ')
      PsiPerlStatementImpl(Perl5: STATEMENT)
        PsiPerlPrintExprImpl(Perl5: PRINT_EXPR)
          PsiElement(Perl5: say)('say')
          PsiWhiteSpace(' ')
          PsiPerlCallArgumentsImpl(Perl5: CALL_ARGUMENTS)
            PsiPerlStringSqImpl(Perl5: STRING_SQ)
              PsiElement(Perl5: QUOTE_SINGLE_OPEN)(''')
              PerlStringContentElementImpl(Perl5: STRING_CONTENT)('hi')
              PsiElement(Perl5: QUOTE_SINGLE_CLOSE)(''')
      PsiWhiteSpace('\n  ')
      PsiElement(Perl5: })('}')
  PsiWhiteSpace('\n')
  PsiErrorElement:<func definition>, <method definition>, <statement>, <sub definition>, Perl5: #@abstract, Perl5: #@deprecated, Perl5: #@inject, Perl5: #@method, Perl5: #@noinspection, Perl5: #@override, Perl5: #@returns, Perl5: #@type, Perl5: #@unknown, Perl5: BLOCK_NAME, Perl5: POD, Perl5: TryCatch::, Perl5: __DATA__, Perl5: __END__, Perl5: case, Perl5: continue, Perl5: default, Perl5: for, Perl5: foreach, Perl5: format, Perl5: fp_after, Perl5: fp_around, Perl5: fp_augment, Perl5: fp_before, Perl5: given, Perl5: if, Perl5: nyi, Perl5: package, Perl5: switch, Perl5: unless, Perl5: until, Perl5: when, Perl5: while or Perl5: { expected, got '}'
    PsiElement(Perl5: })('}')
  PsiWhiteSpace('\n')
